(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "aspectratio=43" "11p" "handout")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("tcolorbox" "many") ("mdframed" "framemethod=tikz")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "FiraSans"
    "tcolorbox"
    "graphicx"
    "listings"
    "booktabs"
    "caption"
    "color"
    "hyperref"
    "mdframed"
    "fontawesome5")
   (TeX-add-symbols
    '("SubItem" 1)
    '("adv" 1)))
 :latex)

